from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, FormField, FieldList, TextField, Form


class TestForm(FlaskForm):
    test = StringField()
    submit = SubmitField('OK')


class AddressEntryForm(Form):
    name = TextField()

class AddressesForm(Form):
    """A form for one or more addresses"""
    addresses = FieldList(FormField(AddressEntryForm), min_entries=1)


class FormList(FlaskForm):
    fields = FieldList(StringField())
    submit = SubmitField('OK')