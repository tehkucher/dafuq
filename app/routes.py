from app import app
from app.forms import TestForm, AddressesForm, FormList
from flask import render_template, redirect, flash

@app.route('/', methods=['POST', 'GET'])
@app.route('/index', methods=['POST', 'GET'])
def index():
    form = TestForm()
    if form.validate_on_submit():
        flash(f'data: \n {form.test.data}')
        return redirect('/index')
    return render_template('index.html', form=form)



@app.route('/test', methods=['POST', 'GET'])
def test():
    a = [{'name': 'hui'}, {'name': 'pizda'}]
    form = FormList(fields=a)
    return render_template('test.html', form=form)



